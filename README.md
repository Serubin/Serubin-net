# Serubin.net

This is the public source code for Solomon Rubin's Serubin.net. It serves as an online portfolio and resume.

The site is heavily based on HTML5UP's Twenty design. It has been heavily modified to satisfy the needs of this website.

Please direct questions and comments to the issues section.
